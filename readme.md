### Set up
 * install homstead 
 * run `composer update`
 * run `php artisan key:generate`
 * run `npm install`
 * run `npm install -g gulp`
 * run `npm install -g bower`
 * run `gulp install`
 * run `gulp`

### License
[MIT license](http://opensource.org/licenses/MIT).