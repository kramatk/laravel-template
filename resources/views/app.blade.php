<!DOCTYPE html>
<html lang="en" ng-app="masterApp">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.ico')}}"/>

    <title>Master Application</title>

    <link href="{{asset('lib/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{asset('lib/css-spinners/css/spinners.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{asset('css/all.min.css')}}" rel="stylesheet" type="text/css" />    
    
    <script src="{{asset('lib/angular/angular.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('lib/angular-route/angular-route.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('lib/angular-bootstrap/ui-bootstrap.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('lib/angular-bootstrap/ui-bootstrap-tpls.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('lib/angular-animate/angular-animate.min.js')}}" type="text/javascript"></script>    
    <!-- <script src="{{asset('lib/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>     -->
    <script src="{{asset('js/app.min.js')}}" type="text/javascript"></script>

</head>
<body ng-controller="MainController as mainCtrl">
    <div ng-view></div>
</body>
</html>