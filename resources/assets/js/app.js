angular.module('masterApp', ['ngRoute',"ngAnimate",'ui.bootstrap'])
.constant('URL_CONSTANT',{
    BASE_URL:"",
    HOST_NAME:""

})
.config(["$routeProvider","$locationProvider","URL_CONSTANT",function($routeProvider,$locationProvider,URL_CONSTANT) {
    // $routeProvider
    //     .when(URL_CONSTANT+"/", {
    //         templateUrl : URL_CONSTANT.HOST_NAME+'/template/index.html',
    //         controller  : 'indexController',
    //         controllerAs: 'ctrl'
    //     })
    $locationProvider.html5Mode(true);
}]);